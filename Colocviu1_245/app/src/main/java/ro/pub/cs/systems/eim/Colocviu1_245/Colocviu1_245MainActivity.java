package ro.pub.cs.systems.eim.Colocviu1_245;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Colocviu1_245MainActivity extends AppCompatActivity {

    private EditText inputEditText;
    private Button addButton;
    private Button computeButton;
    private TextView allTermsTextView;

    private ButtonClickListener buttonClickListener = new ButtonClickListener();
    private class ButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.add_button) {
                if (inputEditText != null) {
                    if (allTermsTextView.getText().toString().length() > 0) {
                        String input = inputEditText.getText().toString();
                        String currentResult = allTermsTextView.getText().toString();
                        String finalResult = currentResult + "+" + input;
                        allTermsTextView.setText(finalResult);
                    } else {
                        String input = inputEditText.getText().toString();
                        allTermsTextView.setText(input);
                    }


                }
            } else if (view.getId() == R.id.compute_button) {

            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practical_test01_245_main);

//    initializare elemente
        inputEditText = (EditText)findViewById(R.id.input_edit_text);
        addButton = (Button)findViewById(R.id.add_button);
        computeButton = (Button)findViewById(R.id.compute_button);
        allTermsTextView = (TextView)findViewById(R.id.all_terms_text_view);

//        asociere listener la butoane
        addButton.setOnClickListener(buttonClickListener);
        computeButton.setOnClickListener(buttonClickListener);

    }

}
